const fs = require('fs');
const zc = require('zcompile');

let args = process.argv.slice(2);
let src = __dirname + '/src';
let dst = args.find(arg => /^\-o=.+$/.test(arg));
dst = dst ? dst.slice(3) : __dirname + '/dist';
let debug = args.includes("debug");


require('zcompile')({
	source: src,
	destination: dst,

	files: fs.readdirSync(src),
	minifySelectors: false,
	minifyJS: {
		debug: debug,
	},
});