(function(undefined) {
    "use strict";

    var createCleanObject = function() {
        return Object.create(null);
    };
    var parsePath = function(path) {
        if (!path) {
            return [''];
        }
        var regexpMatches = /^\/+(.*?)\/*$/.exec(path);
        if (!regexpMatches) {
            return null;
        }
        return regexpMatches[1].split(/\/+/);
    };
    var clone = function(data) {
        var ret;
        if (Array.isArray(data)) {
            ret = data.slice().map(clone);
        } else if (data && data.constructor == Object) {
            ret = Object.assign(createCleanObject(), data);
            for (var key in data) {
                ret[key] = clone(ret[key]);
            }
        } else {
            ret = data;
        }
        return ret;
    };
    var createRouteObj = function() {
        var ret = createCleanObject();
        ret.literals = createCleanObject();
        ret.params = createCleanObject();
        return ret;
    };
    var getParamTypeFromCode = function(code) {
        switch (code) {
            case 'u':
                return "natural";

            case 'i':
                return "integer";

            case 'f':
                return "float";

            case 's':
                return "string";

            default:
                throw new Error("Invalid parameter type");
        }
    };
    var isNaturalString = function(str) {
        return /^[0-9]+$/.test(str);
    };
    var isIntegerString = function(str) {
        return /^-?[0-9]+$/.test(str);
    };
    var isFloatString = function(str) {
        return /^-?[0-9]*\.?[0-9]+$/.test(str);
    };

    /*
        If:

            Modem.addComponent('component1');
            Modem.addRoute('/component1/p/a/t/h', metadata);
            Modem.addRoute('/component1/p/i{paramName}/t', metadata);
            Modem.addRoute('/component1/p/f{paramName}/s{anotherParam}', metadata);

        Then:

        routes = {
            component1: {
                literals: {
                    p: {
                        literals: {
                            a: {
                                literals: {
                                    t: {
                                        literals: {
                                            h: {
                                                end: metadata
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        params: {
                            integer: {
                                name: "paramName",
                                literals: {
                                    t: {
                                        end: metadata,
                                    }
                                }
                            },
                            float: {
                                name: "paramName",
                                params: {
                                    string: {
                                        name: "anotherParam",
                                        end: metadata
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    */
    var routes = createCleanObject();

    var Modem = createCleanObject();

    Modem.addComponent = function(componentName) {
        routes[componentName] = createRouteObj();
    };

    Modem.addRoute = function(path, metadata) {
        if (metadata === undefined) {
            metadata = null;
        }

        var pathParts = parsePath(path);
        if (!pathParts) {
            throw new SyntaxError('"' + path + '" is not a valid path');
        }

        var component = pathParts.shift();
        var currentRouteObj = routes[component];

        if (!currentRouteObj) {
            throw new SyntaxError('Attempted to add route with non-existent component "' + component + '"');
        }

        while (pathParts.length) {
            var part = pathParts.shift();
            var regexpMatches;
            if (regexpMatches = /^\s*([uifs])\s*\{\s*([A-Za-z0-9]+)\s*\}\s*$/.exec(part)) {
                var paramName = regexpMatches[2];
                var paramType = getParamTypeFromCode(regexpMatches[1]);
                if (currentRouteObj.params[paramType]) {
                    if (currentRouteObj.params[paramType].name !== paramName) {
                        throw new SyntaxError('A parameter with the same type already exists');
                    }
                    currentRouteObj = currentRouteObj.params[paramType];
                } else {
                    currentRouteObj.params[paramType] = createRouteObj();
                    currentRouteObj.params[paramType].name = paramName;
                    currentRouteObj = currentRouteObj.params[paramType];                    
                }
            } else {
                if (!currentRouteObj.literals[part]) {
                    currentRouteObj.literals[part] = createRouteObj();
                }

                currentRouteObj = currentRouteObj.literals[part];
            }
        }

        currentRouteObj.end = metadata;
    };

    var construct404 = function() {
        return {
            error: Modem.ERROR_NOT_FOUND,
        };
    };

    Modem.route = function(route) {
        var parameters = createCleanObject();
        var parts = parsePath(route);
        if (!parts) {
            return {
                error: Modem.ERROR_BAD_URL,
            };
        }

        var component = parts.shift();
        if (component === '') {
            return {
                error: Modem.ERROR_NO_COMPONENT,
            };
        }

        var currentRouteObj = routes[component];
        if (currentRouteObj === undefined) {
            // Component doesn't exist
            return construct404();
        }

        var partsAfterComponent = parts.slice();

        while (parts.length) {
            var part = parts.shift();

            if (currentRouteObj.literals[part]) {
                currentRouteObj = currentRouteObj.literals[part];
            }

            else if (currentRouteObj.params.natural && isNaturalString(part)) {
                parameters[currentRouteObj.params.natural.name] = parseInt(part, 10);
                currentRouteObj = currentRouteObj.params.natural;
            }

            else if (currentRouteObj.params.integer && isIntegerString(part)) {
                parameters[currentRouteObj.params.integer.name] = parseInt(part, 10);
                currentRouteObj = currentRouteObj.params.integer;
            }

            else if (currentRouteObj.params.float && isFloatString(part)) {
                parameters[currentRouteObj.params.float.name] = parseFloat(part);
                currentRouteObj = currentRouteObj.params.float;
            }

            else if (currentRouteObj.params.string) {
                parameters[currentRouteObj.params.string.name] = part;
                currentRouteObj = currentRouteObj.params.string;
            }

            else {
                // No route matched, return 404
                return construct404();
            }
        }

        // Check for terminal property
        var metadata = currentRouteObj.end;
        if (metadata === undefined) {
            return construct404();
        }

        if (typeof metadata == "function") {
            metadata = metadata(parameters, partsAfterComponent, component);
        }
        metadata = clone(metadata);

        return {
            component: component,
            metadata: metadata,
            parameters: parameters,
            parts: partsAfterComponent,
        };
    };

    Modem.ERROR_BAD_URL = 400;
    Modem.ERROR_NO_COMPONENT = 307;
    Modem.ERROR_NOT_FOUND = 404;

    Object.freeze(Modem);

    if (typeof exports == "object") {
        module.exports = Modem;
    } else {
        window.Modem = Modem;
    }
})();
